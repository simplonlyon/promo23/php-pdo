<?php
use App\Entities\Dog;
use App\Entities\Person;
use App\Repository\DogRepository;
use App\Repository\PersonRepository;

require '../vendor/autoload.php';

$repo = new DogRepository();
$firstnames = ['Johnathon', 'Anthony', 'Erasmo', 'Raleigh', 'Nancie', 'Tama', 'Camellia', 'Augustine', 'Christeen', 'Luz', 'Diego', 'Lyndia', 'Thomas', 'Georgianna', 'Leigha', 'Alejandro', 'Marquis', 'Joan', 'Stephania', 'Elroy', 'Zonia', 'Buffy', 'Sharie', 'Blythe', 'Gaylene', 'Elida', 'Randy', 'Margarete', 'Margarett', 'Dion', 'Tomi', 'Arden', 'Clora', 'Laine', 'Becki', 'Margherita', 'Bong', 'Jeanice', 'Qiana', 'Lawanda', 'Rebecka', 'Maribel', 'Tami', 'Yuri', 'Michele', 'Rubi', 'Larisa', 'Lloyd', 'Tyisha', 'Samatha'];
$lastnames = ['Mischke', 'Serna', 'Pingree', 'Mcnaught', 'Pepper', 'Schildgen', 'Mongold', 'Wrona', 'Geddes', 'Lanz', 'Fetzer', 'Schroeder', 'Block', 'Mayoral', 'Fleishman', 'Roberie', 'Latson', 'Lupo', 'Motsinger', 'Drews', 'Coby', 'Redner', 'Culton', 'Howe', 'Stoval', 'Michaud', 'Mote', 'Menjivar', 'Wiers', 'Paris', 'Grisby', 'Noren', 'Damron', 'Kazmierczak', 'Haslett', 'Guillemette', 'Buresh', 'Center', 'Kucera', 'Catt', 'Badon', 'Grumbles', 'Antes', 'Byron', 'Volkman', 'Klemp', 'Pekar', 'Pecora', 'Schewe', 'Ramage'];

$repoPerson = new PersonRepository();

// for ($i = 0; $i < 100; $i++) {
//     $repoPerson->persist(new Person($firstnames[array_rand($firstnames, 1)], $lastnames[array_rand($lastnames, 1)]));

// }
var_dump($repoPerson->findAll(2, 20));



// $dog = new Dog('Test', 'test', new DateTime());
// var_dump($dog);

// $repo->persist($dog);

// var_dump($dog);

// var_dump($repo->findAll());

// $dog = $repo->findById(1);
// var_dump($dog);