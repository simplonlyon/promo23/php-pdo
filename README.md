# php-pdo

Projet dans lequel on fait du PDO avec du PHP sans framework

## How to use
1. Cloner le projet
2. Faire un `composer install` pour télécharger les dépendances et créer le autoload.php
3. Créer une base de donnée p23_first et dans cette ci importer le contenu de [database.sql](database.sql) (on peut faire avec `sudo mysql p23_first < database.sql`)
4. Lancer le projet avec F5 -> built in server, ou bien avec `php -S localhost:8000 -t public`
5. Accéder à http://localhost:8000


## Exercices
### DogRepository, le findAll
1. Dans le dossier src, créer un dossier Entities et dans ce dossier, créer une nouvelle classe Dog (on oublie pas son namespace) qui aura les différentes propriétés de la table dog, toute en privées, avec des getter/setters, et un contructeur (pour la date, le type ça sera \DateTime)
2. Dans le dossier src, créer un dossier Repository et dedans créer une classe DogRepository
3. Dans cette classe DogRepository, créer une méthode findAll() qui va renvoyer un array (un Dog[] pour être exact, mais ça on peut le dire que dans le doc) et dans cette méthode, reprendre ce qu'on a fait ensemble dans le index.php, avec la connexion, le prepare, le execute et tout
4. Dans le findAll toujours, faire une boucle sur le résultat de la requête, et pour chaque résultat, créer une instance de la classe Dog puis mettre cette instance à l'intérieur d'un tableau (créé tout en haut de la méthode) qu'on fera en sorte de return à la fin
5. Dans le index.php, on dégage tout, on met le require vendor, on fait une instance de notre repository et on tente d'appeler sa méthode findAll() et pourquoi pas en faire un var_dump 

### Méthode persist par étape
1. Créer une méthode persist dans le DogRepository, qui va créer ou récupérer une connexion et s'en servir pour préparer une requête INSERT INTO en dur (genre elle fait persister tout le temps "médor" le "dalmatien" né le "2020-10-12"). Essayer de lancer cette méthode depuis l'index une fois ou deux et vérifier dans la bdd si ça fait bien persister médor le chien
2. Dans la méthode persist, rajouter 3 arguments $name,$breed et $birthdate et faire en sorte de modifier la requête pour y caler ces variables plutôt que les valeurs en dur. On retente de lancer la méthode en lui donnant des arguments différents
3. On remplace les 3 arguments par un seul, une instance de Dog 

(faire en sorte d'utiliser les bindValue pour assigner des paramètres à la requête et se prémunir des injections SQL)

### Faire le findById
on peut s'inspirer de la méthode findAll, pasque c'est un peu la même requête dans le prepare, à la différence qu'il y aura un where et un paramètre dans la requête, donc un bindValue (faire en sorte que la fonction findById attende un $id en argument également)

Petite différence avec le findAll, c'est que quand on cherche un truc par son id, ça ne va nous renvoyer que un seul ou aucun résultat, donc on pourra faire un fetch() à la place d'un fetchAll() et qu'on aura donc pas à faire une boucle

(Dans l'idéal, on aimerait que quand on appelle findById, ça nous renvoie soit une instance de Dog, soit null si aucun chien n'a été trouvé pour l'id donné à la requête)