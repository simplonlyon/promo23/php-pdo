<?php

namespace App\Entities;

class Person {
    private ?int $id;
    private string $firstname;
    private string $name;


    /**
     * @param int|null $id
     * @param string $firstname
     * @param string $name
     */
    public function __construct(string $firstname, string $name, ?int $id = null) {
    	$this->id = $id;
    	$this->firstname = $firstname;
    	$this->name = $name;
    }

	/**
	 * @return int|null
	 */
	public function getId(): ?int {
		return $this->id;
	}
	
	/**
	 * @param int|null $id 
	 * @return self
	 */
	public function setId(?int $id): self {
		$this->id = $id;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getFirstname(): string {
		return $this->firstname;
	}
	
	/**
	 * @param string $firstname 
	 * @return self
	 */
	public function setFirstname(string $firstname): self {
		$this->firstname = $firstname;
		return $this;
	}
	
	/**
	 * @return string
	 */
	public function getName(): string {
		return $this->name;
	}
	
	/**
	 * @param string $name 
	 * @return self
	 */
	public function setName(string $name): self {
		$this->name = $name;
		return $this;
	}
}