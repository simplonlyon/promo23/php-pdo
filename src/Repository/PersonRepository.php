<?php

namespace App\Repository;
use App\Entities\Person;
use PDO;


class PersonRepository {
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }
    /**
     * Summary of findAll
     * @return Person[] les personnes de la base de données
     */
    public function findAll(int $page = 1, int $pageSize = 15):array {
        $persons = [];
        $offset = ($page-1) * $pageSize ;
        $statement = $this->connection->prepare('SELECT * FROM person LIMIT :pageSize OFFSET :page');
        $statement->bindValue('pageSize', $pageSize, PDO::PARAM_INT);
        $statement->bindValue('page', $offset, PDO::PARAM_INT);
        $statement->execute();

        foreach($statement->fetchAll() as $line) {
            $persons[] = new Person($line['firstname'], $line['name'], $line['id']);
        }

        return $persons;
    }
    /**
     * Summary of persist
     * @param Person $person la personne à faire persister en bdd
     */
    public function persist(Person $person) {

        $statement = $this->connection->prepare('INSERT INTO person (name, firstname) VALUES (:name,:firstname)');

        $statement->execute([
            'name' => $person->getName(),
            'firstname' => $person->getFirstname()
        ]);

        $person->setId($this->connection->lastInsertId());
    }
    
}