<?php

namespace App\Repository;

use App\Entities\Dog;
use DateTime;
use PDO;

/**
 * Un Repository, ou DAO (Data Access Object) est une
 * classe dont le but est de concentrer les appels à la base de données,
 * ainsi, le reste de l'application dépendra des repositories et on aura pas 
 * des appels bdd disseminés partout dans l'appli (comme ça, si jamais 
 * la table change, ou le sgbd ou autre, on aura juste le repo à 
 * modifier et pas tous les endroits où on aurait fait ces appels)
 */
class DogRepository
{
    private PDO $connection;

    public function __construct() {
        $this->connection = Database::connect();
    }

    /**
     * Faire une requête SQL vers la base de données et convertir les résultats de
     * cette requêtes en instances de la classe/entité Dog
     * @return Dog[]
     */
    public function findAll(): array
    {
        /** @var Dog[] */
        $dogs = [];


        $statement = $this->connection->prepare('SELECT * FROM dog');

        $statement->execute();

        $results = $statement->fetchAll();
        foreach ($results as $item) {
            $dogs[] = $this->sqlToDog($item);
        }
        return $dogs;
    }

    /**
     * Méthode permettant de faire persister une instance de Dog sur la base de données
     * @param Dog $dog le chien à faire persister
     * @return void Aucun retour, mais une fois persisté, le chien aura un id assigné
     */
    public function persist(Dog $dog) {
        $statement = $this->connection->prepare('INSERT INTO dog (name,breed,birthdate) VALUES (:name, :breed, :birthdate)');
        $statement->bindValue('name', $dog->getName());
        $statement->bindValue('breed', $dog->getBreed());
        $statement->bindValue('birthdate', $dog->getBirthdate()->format('Y-m-d'));


        $statement->execute();

        $dog->setId($this->connection->lastInsertId());

    }

    /**
     * Méthode qui va renvoyer un Dog en se basant sur son id, ou null si l'id ne
     * correspond à aucun chien en bdd
     * @param int $id l'id du chien qu'on cherche
     * @return Dog|null L'instance de chien correspondante, ou null si pas de correspondance
     */
    public function findById(int $id):?Dog {
        $statement = $this->connection->prepare('SELECT * FROM dog WHERE id=:id');
        $statement->bindValue('id', $id);

        $statement->execute();

        $result = $statement->fetch();
        if($result) {
            return $this->sqlToDog($result);
        }
        return null;
    }

    /**
     * Méthode qui prend un ligne de résultat PDO et la convertit en instance 
     * de Dog
     * @param array $line la ligne de résultat à convertir, sous forme de tableau associatif
     * @return Dog l'instance de chien
     */
    private function sqlToDog(array $line):Dog {
        $birthdate = null;
        if(isset($line['birthdate'])){
            $birthdate = new DateTime($line['birthdate']);
        }
        //ou bien avec un tertiaire
        //$birthdate = isset($line['birthdate']) ? new DateTime($line['birthdate']):null;
        return new Dog($line['name'], $line['breed'], $birthdate, $line['id']);
    }
}